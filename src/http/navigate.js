var router = require('./router');

module.exports = function navigate(url, state) {
  history.pushState(state, '', url);
  router.navigate(url);
};
