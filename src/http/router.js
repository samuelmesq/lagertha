var NOT_FOUND = /^404NOTFOUND$/;
var routes = [];

function getUrlFragment(url) {
  var fragment = decodeURI(url)
        .toString()
        .replace(/\/$/, '')
        .replace(/^\//, '');

  return fragment ? fragment : '/';
}

function navigate(url) {
  var fragment = getUrlFragment(url);
  var dispatch = function _dispatch(route) {
    var match = fragment.match(route.path);
    if (match) match.shift();
    route.listener.apply(route.listener, match);
  };
  var notFound = function _notFound(route) {
    return route.path.test('404NOTFOUND');
  };

  var founds = routes.filter( function (route) { return route.path.test(fragment); });

  if (founds.length == 0) {
    routes.filter(notFound).forEach(dispatch);
  } else {
    founds.forEach(dispatch);
  }
}

function on(path, listener) {
  routes.push({ path: path, listener: listener });
}

module.exports = { on: on, navigate: navigate, NOT_FOUND: NOT_FOUND };
