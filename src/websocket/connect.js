var uuid4 = require('../hash/uuid4');
var ee    = require('../event/bus');

var CLOSE = 'lagertha:websocket:has_close';
var OPEN = 'lagertha:websocket:has_open';
var SEND = 'lagertha:websocket:send_message';

ee.on(SEND, send);

function connect(url) {
  var _uid = uuid4();
  var listener = false;
  var ws = false;

  var createConnection = function createConnection() {
    ws = new WebSocket(url);
    ws.onclose = onClose;
    ws.onmessage = onMessage;
    ws.onopen = onOpen;
  };

  var onClose = function onClose(event) {
    setTimeout(createConnection, 2000);
    return ee.trigger(CLOSE);
  };

  var onMessage = function onMessage(event) {
    return ee.trigger(_uid, event.data);
  };

  var onOpen = function onOpen(event) {
    return ee.trigger(OPEN, event.target);
  };

  function sendMessage(message) {
    return ee.trigger(SEND, ws, JSON.stringify(message));
  }

  setTimeout(createConnection, 0);

  return {
    sendMessage: sendMessage,
    set onMessage (fn) {
      if (listener) {
        ee.off(_uid, listener);
      }
      ee.on(_uid, fn);
      listener = fn;
    }
  };
}

function send(connection, message) {
  if (connection && connection.readyState == WebSocket.OPEN)
    return connection.send(message);

  ee.one(OPEN, (connection) => {
    ee.trigger(SEND, connection, message);
  });
}

module.exports = connect;
