var events = {};

function eventListeners(event) {
  events[event] = events[event] || [];
  return events[event];
}

function off(event, handler) {
  if (events[event]) {
    events[event] = events[event].filter( function (listener) {
      return listener.future !== handler;
    });
  }
}

function on(event, future, one) {
  one = one || false;
  eventListeners(event).push({ one: one, future: future });
}

function one(event, future) {
  on(event, future, true);
}

function trigger() {
  var args = Array.prototype.slice.call(arguments);
  var event = args.shift();

  events[event] = eventListeners(event).map(function (listener) {
    listener.future.apply(listener.future, args);
    return listener;
  }).filter(function (listener) { return !listener.one; });
}

module.exports = {
  off: off,
  on: on,
  one: one,
  trigger: trigger
};
